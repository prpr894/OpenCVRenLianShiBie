package com.zrsoft.facerecognition;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends BaseActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    private ImageView mImageView;
    private Bitmap mBitmap;
    private FaceDetection mFaceDetection;
    private File mCascadeFile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mImageView = findViewById(R.id.img_face);
        mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.face_min);
//        mImageView.setImageBitmap(mBitmap);
        mFaceDetection = new FaceDetection();
        copyCasecadeFile();
        mFaceDetection.loadCascade(mCascadeFile.getAbsolutePath());

    }

    private void copyCasecadeFile() {
        try {
            // load cascade file from application resources
            InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
            if (mCascadeFile.exists()) {
                return;
            }
            FileOutputStream os = new FileOutputStream(mCascadeFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void btnOnClick(View view) {
        showProgress("识别中...");
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (mFaceDetection.saveInfo(mBitmap) == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mImageView != null) {
                                mImageView.setImageBitmap(mBitmap);
                            }
                        }
                    });
                    hideProgress();
                }else {
                    hideProgress();
                }
            }
        }).start();
    }
}
