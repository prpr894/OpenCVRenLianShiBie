package com.zrsoft.facerecognition;

import android.graphics.Bitmap;

public class FaceDetection {
    static {
        System.loadLibrary("native-lib");
    }
    public native int saveInfo(Bitmap bitmap) ;
    public native  void loadCascade(String filePath);
}
