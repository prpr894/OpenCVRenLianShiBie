#include <jni.h>
#include <opencv2/opencv.hpp>
#include <android/bitmap.h>

//使用命名空间
using namespace cv;


extern "C"
{
JNIEXPORT jint JNICALL
Java_com_zrsoft_facerecognition_FaceDetection_saveInfo(JNIEnv *env, jobject instance,
                                                       jobject bitmap);
//bitmap转成mat
void bitmap2Mat(JNIEnv *env, Mat &mat, jobject bitmap);
//mat转成bitmap
void mat2bitmap(JNIEnv *env, Mat src, jobject bitmap);

JNIEXPORT void JNICALL
Java_com_zrsoft_facerecognition_FaceDetection_loadCascade(JNIEnv *env, jobject instance,
                                                          jstring filePath_);
}

void bitmap2Mat(JNIEnv *env, Mat &mat, jobject bitmap) {
    AndroidBitmapInfo info;
    void *pixels;

    AndroidBitmap_getInfo(env, bitmap, &info);
    //锁定android bitmap 画布
    AndroidBitmap_lockPixels(env, bitmap, &pixels);
    //指定mat的宽高和type
    mat.create(info.height, info.width, CV_8UC4);

    if (info.format == ANDROID_BITMAP_FORMAT_RGBA_8888) {
        Mat temp(info.height, info.width, CV_8UC4, pixels);
        temp.copyTo(mat);
    } else if (info.format == ANDROID_BITMAP_FORMAT_RGB_565) {
        Mat temp(info.height, info.width, CV_8UC2, pixels);
        cvtColor(temp, mat, COLOR_BGR5652BGRA);
    }
    //其他格式需要自己转



    //解锁android bitmap 画布
    AndroidBitmap_unlockPixels(env, bitmap);

}

CascadeClassifier cascadeClassifier;

JNIEXPORT void JNICALL
Java_com_zrsoft_facerecognition_FaceDetection_loadCascade(JNIEnv *env, jobject instance,
                                                          jstring filePath_) {
    const char *filePath = env->GetStringUTFChars(filePath_, 0);

    cascadeClassifier.load(filePath);
    env->ReleaseStringUTFChars(filePath_, filePath);
}

void mat2bitmap(JNIEnv *env, Mat src, jobject bitmap) {
    // 1. 获取图片的宽高，以及格式信息
    AndroidBitmapInfo info;
    AndroidBitmap_getInfo(env, bitmap, &info);
    void *pixels;
    AndroidBitmap_lockPixels(env, bitmap, &pixels);

    if (info.format == ANDROID_BITMAP_FORMAT_RGBA_8888) {
        Mat tmp(info.height, info.width, CV_8UC4, pixels);
        if (src.type() == CV_8UC1) {
            cvtColor(src, tmp, COLOR_GRAY2RGBA);
        } else if (src.type() == CV_8UC3) {
            cvtColor(src, tmp, COLOR_RGB2RGBA);
        } else if (src.type() == CV_8UC4) {
            src.copyTo(tmp);
        }
    } else {
        // info.format == ANDROID_BITMAP_FORMAT_RGB_565
        Mat tmp(info.height, info.width, CV_8UC2, pixels);
        if (src.type() == CV_8UC1) {
            cvtColor(src, tmp, COLOR_GRAY2BGR565);
        } else if (src.type() == CV_8UC3) {
            cvtColor(src, tmp, COLOR_RGB2BGR565);
        } else if (src.type() == CV_8UC4) {
            cvtColor(src, tmp, COLOR_RGBA2BGR565);
        }
    }

    AndroidBitmap_unlockPixels(env, bitmap);
}

JNIEXPORT  jint JNICALL
Java_com_zrsoft_facerecognition_FaceDetection_saveInfo(JNIEnv *env, jobject instance,
                                                       jobject bitmap) {
    //人脸检测 opencv关键类Mat，opencv是C和CPP写的，没有bitmap，只会处理Mat。android里面是bitmap
    //1、bitmap转成bitmap转成OpenCv能操作的cpp对象Mat。mat是一个矩阵
    Mat mat;
    bitmap2Mat(env, mat, bitmap);
    //处理灰度图
    Mat gray_mat;
    cvtColor(mat, gray_mat, COLOR_BGRA2GRAY);
    //再次处理 直方均衡补偿
    Mat equalize_mat;
    equalizeHist(gray_mat, equalize_mat);

    //识别人脸
    std::vector<Rect> faces;
    cascadeClassifier.detectMultiScale(equalize_mat, faces, 1.1, 5);
    //cascadeClassifier.detectMultiScale(equalize_mat,faces,1.1,5,0,Size(150,150));
    if (faces.size() == 1) {
        Rect faceRect = faces[0];
        //人脸部位标识一下
        rectangle(mat, faceRect, Scalar(255, 155, 155), 4);
        mat2bitmap(env, mat, bitmap);
        Mat face_info_mat(equalize_mat,faceRect);
    }


    //把mat放到bitmap中去
//    mat2bitmap(env, equalize_mat, bitmap);


    return 0;

}

